package fakta.ayu.uts_absensi_pegawai

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso

class CustomAdapter(val context: Context, arrayList: ArrayList<HashMap<String,Any>>) : BaseAdapter() {
    lateinit var db: FirebaseFirestore
    var UID = ""
    val list = arrayList
    val COLLECTION_ABSENSI = "data_absensi"
    val F_PGW_NAMA = "nama"
    val F_PGW_JABATAN = "jabatan"
    val F_PGW_UID = "uid"
    val F_PGW_PHOTO = "photo_url"
    val F_PGW_LOKASI = "lokasi"
    val F_PGW_DATETIME = "waktu"
    var uri = Uri.EMPTY

    inner class ViewHolder() {
        var txNama: TextView? = null
        var txJabatan: TextView? = null
        var txLokasi: TextView? = null
        var txDatetime: TextView? = null
        var imv: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if (convertView == null) {
            var inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row_histori, null, true)

            holder.txNama = view!!.findViewById(R.id.textView10) as TextView
            holder.txDatetime = view!!.findViewById(R.id.textView9) as TextView
            holder.txJabatan = view!!.findViewById(R.id.textView11) as TextView
            holder.txLokasi = view!!.findViewById(R.id.textView13) as TextView
            holder.imv = view!!.findViewById(R.id.imageView5) as ImageView

            view.tag = holder
        } else {
            holder = view!!.tag as ViewHolder
        }

        uri = Uri.parse(list.get(position).get(F_PGW_PHOTO).toString())

        holder.txNama!!.setText(list.get(position).get(F_PGW_NAMA).toString())
        holder.txJabatan!!.setText(list.get(position).get(F_PGW_JABATAN).toString())
        holder.txLokasi!!.setText(list.get(position).get(F_PGW_LOKASI).toString())
        holder.txDatetime!!.setText(list.get(position).get(F_PGW_DATETIME).toString())
        Picasso.get().load(uri).into(holder.imv)

        return view!!
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }
}