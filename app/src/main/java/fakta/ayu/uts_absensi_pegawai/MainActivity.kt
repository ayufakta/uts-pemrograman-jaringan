package fakta.ayu.uts_absensi_pegawai

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (FirebaseAuth.getInstance().uid.isNullOrEmpty()){
            val intent = Intent(this, SigninActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
        btnLogOut.setOnClickListener {
            fbAuth.signOut()
            val intent = Intent(this, SigninActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
        btnAbsen.setOnClickListener {
            val i = Intent(this, AbsensiActivity::class.java)
            startActivity(i)
        }
        btnHistori.setOnClickListener {
            val i = Intent(this, HistoriActivity::class.java)
            startActivity(i)
        }
    }
}