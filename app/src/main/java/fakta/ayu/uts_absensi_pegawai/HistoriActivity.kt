package fakta.ayu.uts_absensi_pegawai

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_histori.*
import kotlinx.android.synthetic.main.row_histori.*

class HistoriActivity : AppCompatActivity() {
    var UID = ""
    val COLLECTION_ABSENSI = "data_absensi"
    val F_PGW_NAMA = "nama"
    val F_PGW_JABATAN = "jabatan"
    val F_PGW_UID = "uid"
    val F_PGW_PHOTO = "photo_url"
    val F_PGW_LOKASI = "lokasi"
    val F_PGW_DATETIME = "waktu"
    lateinit var uri : Uri

    lateinit var db : FirebaseFirestore
    lateinit var adapter : CustomAdapter
    lateinit var allAbsen : ArrayList<HashMap<String, Any>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_histori)

        allAbsen = ArrayList()
        uri = Uri.EMPTY
        db = FirebaseFirestore.getInstance()
    }

    override fun onStart() {
        super.onStart()
        db.collection(COLLECTION_ABSENSI).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("FireStore", e.message.toString())
            showDataAbsen()
        }
    }

    fun showDataAbsen(){
        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            UID = user.uid
        }
        db.collection(COLLECTION_ABSENSI).whereEqualTo("uid", UID)
            .get()
            .addOnSuccessListener { result ->
                allAbsen.clear()
                for(doc in result){
                    val hm = HashMap<String, Any>()
                    hm.set(F_PGW_NAMA, doc.get(F_PGW_NAMA).toString())
                    hm.set(F_PGW_JABATAN, doc.get(F_PGW_JABATAN).toString())
                    hm.set(F_PGW_DATETIME, doc.get(F_PGW_DATETIME).toString())
                    hm.set(F_PGW_LOKASI, doc.get(F_PGW_LOKASI).toString())
                    hm.set(F_PGW_PHOTO, doc.get(F_PGW_PHOTO).toString())
                    allAbsen.add(hm)
                }
                adapter = CustomAdapter(this, allAbsen)
                lsDataPresensi.adapter = adapter
            } .addOnFailureListener {
                Toast.makeText(this, "Data tidak tersedia...", Toast.LENGTH_SHORT).show()
            }
    }
}