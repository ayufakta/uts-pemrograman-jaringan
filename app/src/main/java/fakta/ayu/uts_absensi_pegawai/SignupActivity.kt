package fakta.ayu.uts_absensi_pegawai

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var jabatanAdapter :ArrayAdapter<String>

    val COLLECTION_USER = "user"
    val COLLECTION_JABATAN = "jabatan"
    val F_JBT_NAMA = "jabatan_nama"
    val F_PGW_NAMA = "nama"
    val F_PGW_UID = "uid"
    val F_PGW_JABATAN = "jabatan"
    val db = FirebaseFirestore.getInstance()

    var UID = ""
    var pilihJabatan = ""
    var daftarJabatan = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        btnRegister.setOnClickListener(this)

        jabatanAdapter = ArrayAdapter(this, R.layout.custom_spinner, daftarJabatan)
        spJabatan.adapter = jabatanAdapter
        spJabatan.onItemSelectedListener = itemSelectedJabatan
    }

    override fun onClick(v: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()

        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this,"Username / Password can't be empty", Toast.LENGTH_LONG).show()
        }else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering.....")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Successfully Register",Toast.LENGTH_SHORT).show()

                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating....")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, MainActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            this.startActivity(intent)

                            val user = FirebaseAuth.getInstance().currentUser
                            user?.let {
                                UID = user.uid
                            }

                            saveAllData()
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(
                                this,
                                "Username / Password incorrect",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
                .addOnFailureListener{
                    progressDialog.hide()
                    Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_SHORT).show()
                }
        }
    }

    override fun onStart() {
        super.onStart()
        db.collection(COLLECTION_JABATAN).addSnapshotListener { value, e ->
            if (e != null) Log.d("FireStore", e.message.toString())
            getJabatan()
        }
    }

    val itemSelectedJabatan = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spJabatan.setSelection(0)
            pilihJabatan = daftarJabatan.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJabatan = daftarJabatan.get(position)
        }
    }

    fun getJabatan(){
        db.collection(COLLECTION_JABATAN).get()
            .addOnSuccessListener { result ->
                daftarJabatan.clear()
                daftarJabatan.add("----- Pilih Jabatan -----")
                for (doc in result) {
                    daftarJabatan.add(doc.get("jabatan_nama").toString())
                }
                jabatanAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { exception ->
                Log.e("Firebase",exception.toString())
            }
    }

    fun saveAllData(){
        val hm = HashMap<String, Any>()
        hm.set(F_PGW_NAMA, edPgwNamaReg.text.toString())
        hm.set(F_PGW_UID, UID)
        hm.set(F_PGW_JABATAN, pilihJabatan)

        db.collection(COLLECTION_USER).document().set(hm)
    }
}