package fakta.ayu.uts_absensi_pegawai

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_absensi.*
import kotlinx.android.synthetic.main.activity_main.*
import mumayank.com.airlocationlibrary.AirLocation
import java.util.*

class AbsensiActivity : AppCompatActivity(), OnMapReadyCallback {

    val PICK_PHOTO = 100
    var PHOTO_URI : Uri? = null

    var pgw_lat : Double = 0.0
    var pgw_lng : Double = 0.0
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    var UID = ""

    var bulan = 0
    var tahun = 0
    var hari = 0
    var jam = 0
    var menit = 0

    var bulanchar = ""
    var datetime = ""

    var timejam = ""

    var nama = ""
    var jabatan = ""
    var photo = ""

    val COLLECTION_ABSENSI = "data_absensi"
    val F_PGW_NAMA = "nama"
    val F_PGW_JABATAN = "jabatan"
    val F_PGW_UID = "uid"
    val F_PGW_PHOTO = "photo_url"
    val F_PGW_LOKASI = "lokasi"
    val F_PGW_DATETIME = "waktu"

    val db = FirebaseFirestore.getInstance()

    lateinit var mapFragment : SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absensi)

        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btnUpImg.setOnClickListener {
            getPhotoFromPhone()
        }
        button3.setOnClickListener {
            uploadPhotoToFirebase()
        }
    }

    override fun onStart() {
        super.onStart()
        getIdentitas()
    }

    fun getIdentitas(){
        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            UID = user.uid
        }
        db.collection("user").whereEqualTo("uid", UID)
            .get()
            .addOnSuccessListener { result ->
                for(doc in result){
                    nama = doc.get("nama").toString()
                    jabatan = doc.get("jabatan").toString()
                }
            }.addOnFailureListener { exception ->
                Log.e("Firebase", exception.toString())
            }
    }

    private fun getPhotoFromPhone() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_PHOTO)
    }

    private fun uploadPhotoToFirebase() {
        val progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Upload Foto....")
        progressDialog.show()
        val photoName = UUID.randomUUID().toString()
        val getURL = FirebaseStorage.getInstance().getReference("user/$photoName").child("user/$photoName")

        getURL.putFile(PHOTO_URI!!)
            .continueWithTask { task ->
                getURL.downloadUrl
            }.addOnCompleteListener { task ->
                photo = task.result.toString()
                Log.e("URL", task.toString())
                kirimAbsensi()
                progressDialog.hide()
            }
    }

    fun kirimAbsensi(){

        val cal: Calendar = Calendar.getInstance()

        bulan = cal.get(Calendar.MONTH)
        tahun = cal.get(Calendar.YEAR)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        jam = cal.get(Calendar.HOUR_OF_DAY)
        menit = cal.get(Calendar.MINUTE)

        bulan += 1

        if(bulan == 1){
            bulanchar = "Januari"
        }else if (bulan == 2){
            bulanchar = "Februari"
        }else if (bulan == 3){
            bulanchar = "Maret"
        }else if (bulan == 4){
            bulanchar = "April"
        }else if (bulan == 5){
            bulanchar = "Mei"
        }else if (bulan == 6){
            bulanchar = "Juni"
        }else if (bulan == 7){
            bulanchar = "Juli"
        }else if (bulan == 8){
            bulanchar = "Agustus"
        }else if (bulan == 9){
            bulanchar = "September"
        }else if (bulan == 10){
            bulanchar = "Oktober"
        }else if (bulan == 11){
            bulanchar = "November"
        }else if (bulan == 12){
            bulanchar = "Desember"
        }

        timejam = String.format("%02d:%02d", jam, menit)
        datetime = "$hari - $bulanchar - $tahun | $timejam WIB"

        val progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Kirim data absesni....")
        progressDialog.show()
        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            UID = user.uid
        }
        val hm = HashMap<String, Any>()
        hm.set(F_PGW_NAMA, nama)
        hm.set(F_PGW_JABATAN, jabatan)
        hm.set(F_PGW_LOKASI, textView5.text)
        hm.set(F_PGW_PHOTO, photo)
        hm.set(F_PGW_DATETIME, datetime)
        hm.set(F_PGW_UID, UID)

        db.collection(COLLECTION_ABSENSI).document().set(hm)
            .addOnSuccessListener {
                progressDialog.hide()
                Toast.makeText(this, "Data berhasil terkirim!", Toast.LENGTH_LONG).show()
                finish()
            }.addOnFailureListener {
                Toast.makeText(this, "gagal : ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_PHOTO){
            if (resultCode == Activity.RESULT_OK && data!!.data != null){
                PHOTO_URI = data.data
                btnUpImg.visibility = View.GONE
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, PHOTO_URI)
                imageView4.setImageBitmap(bitmap)
            }
        }

        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap!=null){
            airLoc = AirLocation(this, true, true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@AbsensiActivity,"Gagal mendapatkan lokasi...", Toast.LENGTH_SHORT).show()
                    }
                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude,location.longitude)
                        var latitude = location.latitude
                        var longitude = location.longitude
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi Anda Sekarang"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))

                        pgw_lat = latitude
                        pgw_lng = longitude

                        val address = getAddress(latitude, longitude)
                        textView5.setText(address)
                    }

                })
        }
    }

    fun getAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(this)
        val list = geocoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }
}